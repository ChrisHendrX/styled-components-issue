import React from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Components from 'components/pages/Components';

import 'bootstrap_theme';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route>
          <Switch>
            <Route path='/' exact component={Components} />
          </Switch>
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default App;