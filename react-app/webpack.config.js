const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');
const fs = require('fs');
const TerserPlugin = require('terser-webpack-plugin');
const dotenv = require('dotenv').config({ path: __dirname + '/.env' });

module.exports = (_, argv) => ({
  entry: './src/index.js',
  output: {
    filename: '[name].bundle.js',
  },
  resolve: {
    symlinks: true,
    modules: [path.resolve(__dirname, './src'), 'node_modules'],
    extensions: [".jsx", ".js", ".json"],
    alias: {
      bootstrap_theme: __dirname + "/node_modules/@chris/shared-library/dist/theme.css"
    },
  },

  devServer: {
    open: true,
    port: 3000,
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'public'),
    liveReload: true,
    disableHostCheck: true,
    watchOptions: {
      poll: true,
      followSymlinks: true,
    },
  },

  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ],
  },
  optimization: {
    minimizer: [new TerserPlugin({
      extractComments: false,
    })],
    splitChunks: {
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          reuseExistingChunk: true,
        },
        common: {
          test: /[\\/]src[\\/]components[\\/]/,
          chunks: "all",
          minSize: 0,
        },
      }
    }
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./public/index.html",
      favicon: "./public/favicon.ico"
    }),
  ],
});
