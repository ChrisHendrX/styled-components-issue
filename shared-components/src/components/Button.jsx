import React from "react";
import styled from 'styled-components';

const StyledButton = styled.button`
  background: blue;
  color: white;
`;

function Button() {

  // Not Working
  return (
    <StyledButton type="button">
      Button provided by shared components
    </StyledButton>
  );

  // Working
  // return (
  //   <button type="button">
  //     Button provided by shared components
  //   </button>
  // );
}

export default Button;